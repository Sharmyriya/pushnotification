import React, { Component } from 'react';
import { View, Text, Image, Button } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';

class Calender extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeDate: new Date(),
        };
    }

    months = ["January", "February", "March", "April",
        "May", "June", "July", "August", "September", "October",
        "November", "December"];

    weekDays = [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    ];

    nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    generateMatrix() {
        var matrix = [];
        // Create header
        matrix[0] = this.weekDays;

        var year = this.state.activeDate.getFullYear();
        var month = this.state.activeDate.getMonth();

        var firstDay = new Date(year, month, 1).getDay();
        var maxDays = this.nDays[month];
        if (month == 1) { // February
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                maxDays += 1;
            }
        }
        var counter = 1;
        for (var row = 1; row < 7; row++) {
            matrix[row] = [];
            for (var col = 0; col < 7; col++) {
                matrix[row][col] = -1;
                if (row == 1 && col >= firstDay) {
                    // Fill in rows only after the first day of the month
                    matrix[row][col] = counter++;
                } else if (row > 1 && counter <= maxDays) {
                    // Fill in rows only if the counter's not greater than
                    // the number of days in the month
                    matrix[row][col] = counter++;
                }
            }
        }

        return matrix;
    }
    _onPress = (item) => {
        this.setState(() => {
            if (!item.match && item != -1) {
                this.state.activeDate.setDate(item);
                return this.state;
            }
        });
    }
    changeMonth = (n) => {
        this.setState(() => {
          this.state.activeDate.setMonth(
            this.state.activeDate.getMonth() + n
          )
          return this.state;
        });
    }

    render() {
        var matrix = this.generateMatrix();
        var rows = [];
        rows = matrix.map((row, rowIndex) => {
            var rowItems = row.map((item, colIndex) => {
                return (
                    <View  style={{
                        flex: 1,
                        backgroundColor: 'grey',
                    }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            height: rowIndex == 0 ? 40 : 100,
                            padding: rowIndex == 0 ? 0 : .3,
                            justifyContent: 'center',
                            backgroundColor: 'grey',
                        }}>
                            <Text
                                style={{
                                    flex: 1,
                                    height: rowIndex == 0 ? 40 : 140,
                                    marginTop: rowIndex == 0 ? 40 : 0,
                                    width: rowIndex == 0 ? 40 : 0,
                                    padding: rowIndex == 0 ? 10 : 5,
                                    textAlign: rowIndex == 0 ? 'center' : 'left',
                                    backgroundColor: rowIndex == 0 ? '#ddd' : '#fff',
                                    color: colIndex == 0 ? '#a00' : '#000',
                                    fontWeight: item == this.state.activeDate.getDate()
                                        ? 'bold' : '800'
                                }}
                                onPress={() => this._onPress(item)}>
                                {item != -1 ? item : null}
                            </Text>
                        </View>

                        {item == -1 ? null :
                            <View style={{ alignSelf: 'center', padding:5 }}>
                                <Image
                                    source={rowIndex == 0 ? null : { uri: "https://images.unsplash.com/photo-1587062236321-c15a3d4f8546?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" }}
                                    style={{ height: 30, width: 30 }} >
                                </Image>
                            </View>}
                    </View>
                );
            });
            return (
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                    }}>
                    {rowItems}
                </View>
            );
        });
        return (
            <View style={{ flex: 1,justifyContent:'center', }}>
                <Text style={{
                    fontWeight: 'bold',
                    fontSize: 18,
                    textAlign: 'center'
                }}>
                    {this.months[this.state.activeDate.getMonth()]} &nbsp;
                     {this.state.activeDate.getFullYear()}
                </Text>
                {/* <View style={{ flex: 1 }}> */}

                {/* <DropDownPicker
                        items={this.state.months}
                        containerStyle={{ height: 40, width:"70%",alignSelf:'center' }}
                        style={{
                         borderWidth: 1.5,
                            borderRadius: 6, fontSize: 13, }}                        
                        dropDownStyle={{ backgroundColor: '#fafafa',width:"100%",alignSelf:'center' }}
                        onChangeItem={ (v,index)=>this.changeMonth(v,index)} /> */}
                {/* </View> */}
                <View
                    style={{ flex: 1 }}>
                    {rows}
                </View>
                <Button title="Previous"
            
                    onPress={() => this.changeMonth(-1)} />
                <Button title="Next"
                    onPress={() => this.changeMonth(+1)} />
                
            </View>

        );
    }
}

export default Calender;
