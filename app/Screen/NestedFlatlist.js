import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import CheckBox from 'react-native-check-box'


class NestedFlatlist extends Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.state = {
            data: [
                {
                    'name': 'parent1', 'checked': false,
                    'children1': [
                        {
                            'childName': '1.1', 'checked': false,
                            'children2': [{ 'childName': '2.1.1', 'checked': true }, { 'childName': '2.1.2', 'checked': true }, { 'childName': '2.1.3', 'checked': true }]
                        },
                        {
                            'childName': '1.2', 'checked': false,
                            'children2': [{ 'childName': '2.2.1', 'checked': true }, { 'childName': '2.2.2', 'checked': true }, { 'childName': '2.2.3', 'checked':true }]
                        },
                        {
                            'childName': '1.3', 'checked': false,
                            'children2': [{ 'childName': '2.3.1', 'checked': true }, { 'childName': '2.3.2', 'checked':true}, { 'childName': '2.3.3', 'checked':true}]
                        }]
                },
                {
                    'name': 'parent2', 'checked': false,
                    'children1': [
                        {
                            'childName': '2.1', 'checked': false,
                            'children2': [{ 'childName': '2.4.1', 'checked':true }, { 'childName': '2.4.2', 'checked': true }, { 'childName': '2.4.3', 'checked':true }]
                        },
                        {
                            'childName': '2.2', 'checked': false,
                            'children2': [{ 'childName': '2.5.1', 'checked': true }, { 'childName': '2.5.2', 'checked': true }, { 'childName': '2.5.3', 'checked': true }]
                        },
                        {
                            'childName': '2.3', 'checked': false,
                            'children2': [{ 'childName': '2.6.1', 'checked':true }, { 'childName': '2.6.2', 'checked': true }, { 'childName': '2.6.3', 'checked':true }]
                        }]
                },
                {
                    'name': 'parent3', 'checked': false,
                    'children1': [
                        {
                            'childName': '3.1', 'checked': false,
                            'children2': [{ 'childName': '2.7.1', 'checked': true}, { 'childName': '2.7.2', 'checked':true }, { 'childName': '2.7.3', 'checked': true}]
                        },
                        {
                            'childName': '3.2', 'checked': false,
                            'children2': [{ 'childName': '2.8.1', 'checked': true }, { 'childName': '2.8.2', 'checked': true }, { 'childName': '2.8.3', 'checked': true}]
                        },
                        {
                            'childName': '3.3', 'checked': false,
                            'children2': [{ 'childName': '2.9.1', 'checked':true}, { 'childName': '2.9.2', 'checked': true }, { 'childName': '2.9.3', 'checked': true }]
                        }]
                },
                {
                    'name': 'parent4', 'checked': false,
                    'children1': [
                        {
                            'childName': '4.1', 'checked': false,
                            'children2': [{ 'childName': '2.10.1', 'checked': true }, { 'childName': '2.10.2', 'checked': true }, { 'childName': '2.10.3', 'checked': true }]
                        },
                        {
                            'childName': '4.2', 'checked': false,
                            'children2': [{ 'childName': '2.11.1', 'checked': true}, { 'childName': '2.11.2', 'checked': true }, { 'childName': '2.11.3', 'checked': true }]
                        },
                        {
                            'childName': '4.3', 'checked': false,
                            'children2': [{ 'childName': '2.12.1', 'checked': true }, { 'childName': '2.12.2', 'checked':true }, { 'childName': '2.12.3', 'checked': true }]
                        }]
                },
                {
                    'name': 'parent5', 'checked': false,
                    'children1': [
                        {
                            'childName': '5.1', 'checked': false,
                            'children2': [{ 'childName': '2.13.1', 'checked':true }, { 'childName': '2.13.2', 'checked':true }, { 'childName': '2.13.3', 'checked': true }]
                        },
                        {
                            'childName': '5.2', 'checked': false,
                            'children2': [{ 'childName': '2.14.1', 'checked': true }, { 'childName': '2.14.2', 'checked': true}, { 'childName': '2.14.3', 'checked':true }]
                        },
                        {
                            'childName': '5.3', 'checked': false,
                            'children2': [{ 'childName': '2.15.1', 'checked':true }, { 'childName': '2.15.2', 'checked':true }, { 'childName': '2.15.3', 'checked':true }]
                        }]
                },
                {
                    'name': 'parent6', 'checked': false,
                    'children1': [
                        {
                            'childName': '6.1', 'checked': false,
                            'children2': [{ 'childName': '2.16.1', 'checked':true }, { 'childName': '2.16.2', 'checked':true }, { 'childName': '2.16.3', 'checked':true }]
                        },
                        {
                            'childName': '6.2', 'checked': false,
                            'children2': [{ 'childName': '2.17.1', 'checked': true }, { 'childName': '2.17.2', 'checked': true }, { 'childName': '2.17.3', 'checked': true }]
                        },
                        {
                            'childName': '6.3', 'checked': false,
                            'children2': [{ 'childName': '2.18.1', 'checked': true }, { 'childName': '2.18.2', 'checked': true }, { 'childName': '2.18.3', 'checked':true }]
                        }]
                }

            ],
            temp: [],
            temp2: []

        };
    }
    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    onItemPress = (item, index) => {
        var tempItem = item
        tempItem.checked = !item.checked
        const tempArr = [...this.state.data]
        tempArr[index] = tempItem
        this.setState({
            data: tempArr
        })
    }
    onItemPress1 = (item, index) => {
        var tempItem = item
        this.setState({ temp: item })
        tempItem.checked = !item.checked
        const tempArr = [...this.state.temp]
        tempArr[index] = tempItem
        this.setState({
            temp: tempArr
        })
    }
    onItemPress2 = (item, index) => {
        var tempItem = item
        this.setState({ temp2: item })
        tempItem.checked = !item.checked
        const tempArr = [...this.state.temp2]
        tempArr[index] = tempItem
        this.setState({
            temp2: tempArr
        })
    }

    renderParent = ({ item, index }) => {
        return (
            <View
                style={{
                    padding: 10,
                    backgroundColor: "#F4F6F6",
                    marginHorizontal: 10, marginTop: 10,
                    borderRadius: 10,
                    elevation: 15,
                }} key={item.name} >
                <View style={{ marginTop: 5 }}>
                    <Text> UserId: {item.name}</Text>
                </View>
                <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() => {
                        this.onItemPress(item, index)
                    }}
                    isChecked={item.checked}
                    leftText={"CheckBox"}
                />
                {item.checked == true ?
                    <View style={{ backgroundColor: 'grey',flex:1,padding:5 }}>
                        {this.renderChildren1({ item, index })}
                    </View> : null}

            </View>
        )
    }
    renderChildren1 = ({ item, index }) => {
        return (
            <FlatList
                data={item.children1}
                renderItem={({ item, index }) => this.child1(item, index)}
                keyExtractor={(item, index) => String(index)}
            />
        )

    }
    child1 = (item, index) => {
        return (
            <View
                style={{
                    padding: 10,
                    backgroundColor: "#F4F6F6",
                    marginHorizontal: 10, marginTop: 10,
                    borderRadius: 10,
                    elevation: 15,
                }} key={item.childName}>
                <View style={{ marginTop: 5 }}>
                    <Text> UserId: {item.childName}</Text>
                </View>
                <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() => {
                        this.onItemPress1(item, index)
                    }}
                    isChecked={item.checked}
                    leftText={"CheckBox"}
                />
                    {item.checked == true ?
                    <View style={{ backgroundColor: 'steelblue',padding:10}}>
                        {this.renderChildren2({ item, index })}
                    </View> : null}

            </View>
        )
    }
    renderChildren2 = ({ item, index }) => {

        return (
            <FlatList
                data={item.children2}
                renderItem={({ item, index }) => this.child2(item, index)}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) =>  String(index)}

            />
        )
    }
    child2 = (item, index) => {
        return (
            <View
                style={{
                    backgroundColor: "#F4F6F6",
                    marginHorizontal:10, marginTop: 10,
                    borderRadius: 5,
                    elevation: 5,
                    
                }} >
                <View style={{ marginTop: 5 }}>
                    <Text> UserId: {item.childName}</Text>
                </View>
                <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() => {
                        this.onItemPress2(item, index)
                    }}
                    isChecked={item.checked}
                    leftText={"CheckBox"}
                />
            </View>
        )
    }

    render() {
        console.log(this.state.data)
        return (
                
                <View style={{ flex: 1, }}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) =>
                            this.renderParent({ item, index })
                        }
                        keyExtractor={(item, index) =>  String(index)}
                    />
                </View>
        );
    }
}

export default NestedFlatlist;

