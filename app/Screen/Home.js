import React, { Component } from 'react';
import { View, Text,TouchableOpacity } from 'react-native';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignSelf:'center'}}>
        <Text 
        style={{fontSize:16,fontWeight:'bold',padding:10}}> 
        Home 
        </Text>
        <Text 
        style={{fontSize:16,fontWeight:'bold',padding:10,backgroundColor:'green'}}
        onPress={()=>this.props.navigation.navigate('Profile')}>
        Profile
        </Text>
        <Text 
        style={{fontSize:16,fontWeight:'bold',padding:10,backgroundColor:'red',marginTop:15}}
        onPress={()=>this.props.navigation.navigate('NestedFlatlist')}>
        NestedFlatlist
        </Text>
        <Text 
        style={{fontSize:16,fontWeight:'bold',padding:10,backgroundColor:'steelblue',marginTop:15}}
        onPress={()=>this.props.navigation.navigate('Calender')}>
        Calender
        </Text>
      </View>
    );
  }
}

export default Home;
