import React, { Component, useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { fcmService } from './app/Notification/FCMService';
import { localNotificationService } from './app/Notification/LocalNotificationService';
import{createAppContainer,createSwitchNavigator,NavigationActions} from 'react-navigation';
import{createStackNavigator}from 'react-navigation-stack';
import Home from './app/Screen/Home';
import Profile from './app/Screen/Profile';
import Navigation from './app/Navigation';
import NestedFlatlist from './app/Screen/NestedFlatlist';
const CreateStack=createAppContainer(
  createSwitchNavigator(
    {
      Home:{
        screen:Home
      },
      Profile:{
        screen:Profile
      },
      NestedFlatlist:{
        screen:NestedFlatlist
      }
    },
    {
headerMode:'none',
// initialRouteName:'none'
    }

  )
)
// create a component
const App = () => {

  useEffect(()=>{
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification)
  },[])


  const onRegister = (token) => {
    console.log("[App] Token", token);
  }

  const onNotification = (notify) => {
    // console.log("[App] onNotification", notify);
    const options = {
      soundName: 'default',
      playSound: true,
    }

    localNotificationService.showNotification(
      0,
      notify.notification.title,
      notify.notification.body,
      notify,
      options,
    )
  }

  const onOpenNotification = async (notify) => {
    if(notify!==null)
  {
    console.log('notify', notify);
    let sortnot=notify.notification;
    let data1=sortnot.title;
    let data2=sortnot.body;
    console.log('notify', data1,data2);
    Navigation.navigate('Profile',{'title':data1,'body':data2})
  }
  else{
    Navigation.navigate('Home')

  }
  }




  return (
   
    <CreateStack ref={navigatorRef => {
      Navigation.setTopLevelNavigator(navigatorRef);
    }}/>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default App;